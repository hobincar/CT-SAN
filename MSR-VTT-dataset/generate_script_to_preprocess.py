import json
import os


def generate_script_to_download_video_and_extract_frame():
    with open("./videodatainfo_2017.json", 'r') as fin:
        d = json.load(fin)

    videos = d['videos']
    download_commands = [ "mkdir videos" ]
    extract_frame_commands = [ "mkdir frames" ]
    for video in videos:
        download_cmd = "youtube-dl --geo-bypass-country US --no-part -f 'bestvideo[height<=480]' -o 'videos/video{:05d}.%(ext)s' {}".format(video['id'], video['url'])
        download_commands.append(download_cmd)

        mkdir_frame_cmd = "mkdir frames/video{:05d}".format(video['id'])
        ffmpeg_cmd = "ffmpeg -i videos/video{0:05d}* -ss {1} -t {2} 'frames/video{0:05d}/%d.jpg'".format(video['id'], video['start time'], video['end time'] - video['start time'])
        rm_cmd = "rm videos/video{0:05d}*".format(video['id'])
        rm_dir = "rmdir frames/video{0:05d}*".format(video['id']) # Remove directory if empty
        extract_frame_commands.append(" && ".join([ mkdir_frame_cmd, ffmpeg_cmd, rm_cmd, rm_dir ]))

    with open("./download.sh", 'w') as fout:
        fout.write("\n".join(download_commands))
    os.chmod("./download.sh", 0o755)
    with open("./extract_frame.sh", 'w') as fout:
        fout.write("\n".join(extract_frame_commands))
    os.chmod("./extract_frame.sh", 0o755)

if __name__ == "__main__":
    generate_script_to_download_video_and_extract_frame()
