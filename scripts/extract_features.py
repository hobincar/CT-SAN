import argparse
import json
import os
import sys
sys.path.insert(0, os.path.abspath('.'))

import h5py
import torch
import torch.nn as nn
import torchvision.models as models
from torch.autograd import Variable
from tqdm import tqdm

from videocap.util import imread, imcrop_and_resize


def build_model():
    resnet152 = models.resnet152(pretrained=True)
    modules = list(resnet152.children())[:-2]
    resnet152 = nn.Sequential(*modules)
    for p in resnet152.parameters():
        p.requires_grad = False
    return resnet152


def load_batch(args):
    frames_batch = []
    video_dnames = os.listdir(args.root_dpath_in)
    for video_dname in video_dnames:
        video_dpath = os.path.join(args.root_dpath_in, video_dname)
        frame_fnames = os.listdir(video_dpath)
        skip_frame = max(args.skip_frame, len(frame_fnames) // 40) # The maximum number of frames is 40 per video.
        for frame_index in range(1, len(frame_fnames)+1, skip_frame):
            frame_fpath = os.path.join(video_dpath, "{}.jpg".format(frame_index))
            frame = imread(frame_fpath)
            frame = imcrop_and_resize(frame)
            frame = frame.swapaxes(1, 2).swapaxes(0, 1)
            frames_batch.append(frame)
            if len(frames_batch) == args.batch_size:
                yield frames_batch, video_dname, False
                frames_batch = []
        if len(frames_batch) > 0:
            yield frames_batch, video_dname, True
            frames_batch = []


def extract_features(args):
    resnet152 = build_model()
    resnet152 = resnet152.cuda()
    fout = h5py.File(args.fpath_out)

    features = None
    pbar = tqdm(enumerate(load_batch(args)))
    for i, (frames_batch, video_dname, video_end) in pbar:
        pbar.set_description("Extracting features from {}...".format(video_dname))
        frames_batch = torch.Tensor(frames_batch).cuda()
        features_batch = resnet152(frames_batch).cpu().numpy()
        if len(features_batch) > 0:
            features = features_batch if features is None else np.vstack([features, features_batch])

        if video_end:
            fout.require_group(video_dname)
            fout[video_dname]['res5c'] = features
            # print("Saved {}...".format(video_dname))
            features = None
        fout.flush()
    fout.close()


if __name__ == "__main__":
    a = argparse.ArgumentParser()
    a.add_argument("--root_dpath_in", help="root directory path of frames to load")
    a.add_argument("--fpath_out", help="file path of features to save")
    a.add_argument("--batch_size", help="file path of features to save")
    a.add_argument("--skip_frame", help="The number of frames to skip", type=int)
    args = a.parse_args()

    os.makedirs(os.path.dirname(args.fpath_out), exist_ok=True)
    extract_features(args)

