import argparse
from collections import Counter
import json
import os
import parse
import random

from tqdm import tqdm
import nltk


def save_as_json(obj, fpath):
    with open(fpath, 'w') as fout:
        json.dump(obj, fout)
    print("Saved {}".format(fpath))


def extract_vocabulary(args):
    with open(args.data_in_fpath, 'r') as fin:
        d = json.load(fin)
    sentences = d['sentences']

    total_words = []
    for sentence in tqdm(sentences, desc="Extracting words..."):
        caption = sentence['caption'].strip("./\\")
        words = nltk.word_tokenize(caption)
        salient_words = [ w for w, p in nltk.pos_tag(words) if p in [ 'NN', 'VBP', 'JJ' ] ]
        total_words += salient_words


    word_counter = Counter(total_words)

    top_k = args.vocabulary_top_k
    words_top_k = [ word for word, freq in sorted(word_counter.items(), key=lambda e: -e[1])[:top_k] ]
    indices = []
    word2idx = {}
    idx2word = {}
    for idx, word in enumerate(words_top_k):
        indices.append(idx)
        word2idx[word] = idx
        idx2word[idx] = word
    save_as_json(words_top_k, "{}/words_{}.json".format(args.vocabulary_dpath, top_k))
    save_as_json(indices, "{}/index_{}.json".format(args.vocabulary_dpath, top_k))
    save_as_json(word2idx, "{}/word_to_index_{}.json".format(args.vocabulary_dpath, top_k))
    save_as_json(idx2word, "{}/index_to_word_{}.json".format(args.vocabulary_dpath, top_k))

    words = list(word_counter.keys())
    save_as_json(words, "{}/words.json".format(args.vocabulary_dpath))


def build_MSRVTT_dataframe(args, videos, sentences, output_fpath):
    with open("{}/words_{}.json".format(args.vocabulary_dpath, args.vocabulary_top_k), 'r') as fin:
        words_top_k = json.load(fin)

    videos, sentences, output_fpath = videos[0], sentences[0], output_fpath[0]
    parser = parse.compile("video{:d}")
    videos = sorted(videos, key=lambda x: parser.parse(x['video_id'])[0])
    videos = [ video for video in videos for _ in range(20) ]
    sentences = sorted(sentences, key=lambda x: parser.parse(x['video_id'])[0])
    info_list = []
    pbar = tqdm(zip(videos, sentences))
    for video, sentence in pbar:
        assert video['video_id'] == sentence['video_id']
        pbar.set_description("Processing {}...".format(video['video_id']))

        caption = sentence['caption'].strip("./\\")
        words = nltk.word_tokenize(caption)
        bow = [ w for w, p in nltk.pos_tag(words) if p in [ 'NN', 'VBP', 'JJ' ] and w in words_top_k ]
        info = {
            "key": "vid{}_sen{}".format(video['id'], sentence['sen_id']),
            "description": caption,
            "vid_key": "video{:05d}".format(int(video['id'])),
            "sen_key": sentence['sen_id'],
            "bow": bow,
        }
        info_list.append(info)

    with open(output_fpath, 'w') as fout:
        fout.write("key\tdescription\tvid_key\tsen_key\tbow\n")
        for info in info_list:
            fout.write("{key}\t{description}\t{vid_key}\t{sen_key}\t{bow}\n".format(**info))


def build_dataframe_tvt(args):
    with open(args.data_in_fpath, 'r') as fin:
        d = json.load(fin)
    videos = d['videos']
    sentences = d['sentences']

    # ids = [ video['video_id'] for video in videos ] # Use all videos
    parser = parse.compile("video{:d}")
    video_names = os.listdir(args.frames_dpath)
    ids = [ parser.parse(id) for id in video_names ]
    video_ids = [ "video{}".format(id) for id in ids ] # Use videos that were able to download
    random.shuffle(video_ids)
    n_data = len(video_ids)
    n_train = int(n_data * args.corpus_ratio[0])
    n_val = int(n_data * args.corpus_ratio[1])
    # n_test = n_data - (n_train + n_val)
    id_dict = {
        'train': video_ids[:n_train],
        'val': video_ids[n_train:n_train+n_val],
        'test': video_ids[n_train+n_val:],
    }
    for corpus_type in [ 'train', 'val', 'test' ]:
        _videos = [ video for video in videos if video['video_id'] in id_dict[corpus_type] ],
        _sentences = [ sentence for sentence in sentences if sentence['video_id'] in id_dict[corpus_type] ],
        output_fpath = args.__dict__["{}_data_out_fpath".format(corpus_type)],
        if args.dataset == "MSR-VTT":
            build_MSRVTT_dataframe(args, _videos, _sentences, output_fpath)
        else:
            raise NotImplementedError

def build_dataframe_tt(args):
    with open(args.data_in_fpath, 'r') as fin:
        d = json.load(fin)
    videos = d['videos']
    sentences = d['sentences']

    # ids = [ video['video_id'] for video in videos ] # Use all videos
    parser = parse.compile("video{:d}")
    video_names = os.listdir(args.frames_dpath)
    ids = [ parser.parse(id)[0] for id in video_names ]
    video_ids = [ "video{}".format(id) for id in ids ] # Use videos that were able to download
    random.shuffle(video_ids)
    n_data = len(video_ids)
    n_train = int(n_data * args.corpus_ratio[0])
    # n_test = n_data - (n_train)
    id_dict = {
        'train': video_ids[:n_train],
        'test': video_ids[n_train:],
    }
    for corpus_type in [ 'train', 'test' ]:
        _videos = [ video for video in videos if video['video_id'] in id_dict[corpus_type] ],
        _sentences = [ sentence for sentence in sentences if sentence['video_id'] in id_dict[corpus_type] ],
        output_fpath = args.__dict__["{}_data_out_fpath".format(corpus_type)],
        print("# {} dataset: {}".format(corpus_type, len(_videos[0])))
        if args.dataset == "MSR-VTT":
            build_MSRVTT_dataframe(args, _videos, _sentences, output_fpath)
        else:
            raise NotImplementedError

if __name__ == "__main__":
    a = argparse.ArgumentParser()
    a.add_argument("--dataset", help="Type of dataset")
    a.add_argument("--data_in_fpath", help="a file path of input data")
    a.add_argument("--frames_dpath", help="a directory path of frames")
    a.add_argument("--random_seed", help="Seed for random module for reproducibility", type=int)
    a.add_argument("--corpus_ratio", help="Ratio of dataset corpus(train/val/test or train/test)", type=float, nargs='+')
    a.add_argument("--train_data_out_fpath", help="an output file path of training data")
    a.add_argument("--val_data_out_fpath", help="an output file path of validation data")
    a.add_argument("--test_data_out_fpath", help="an output file path of test data")
    a.add_argument("--vocabulary_dpath", help="an output directory path of vocabulary")
    a.add_argument("--vocabulary_top_k", help="The number of vocabulary to extract", type=int)
    args = a.parse_args()

    extract_vocabulary(args)

    random.seed(args.random_seed)
    if len(args.corpus_ratio) == 2:
        os.makedirs(os.path.dirname(args.train_data_out_fpath), exist_ok=True)
        os.makedirs(os.path.dirname(args.test_data_out_fpath), exist_ok=True)
        build_dataframe_tt(args)
    elif len(args.corpus_ratio) == 3:
        os.makedirs(os.path.dirname(args.train_data_out_fpath), exist_ok=True)
        os.makedirs(os.path.dirname(args.val_data_out_fpath), exist_ok=True)
        os.makedirs(os.path.dirname(args.test_data_out_fpath), exist_ok=True)
        build_dataframe_tvt(args)
    else:
        raise NotImplementedError

