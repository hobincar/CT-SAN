: ' Generating vocabulary from train/test dataset '
python3 scripts/build_vocabulary.py \
    --GloVe_fpath_in dataset/GloVe/glove.840B.300d.txt \
    --GloVe_fpath_out dataset/GloVe/glove.840B.300d.json \
    --vocabulary_top_k 2000 \
    --dataframe_fpath_list dataset/MSR-VTT/DataFrame/MSR-VTT_train.csv dataset/MSR-VTT/DataFrame/MSR-VTT_test.csv \
    --vocabulary_dpath dataset/MSR-VTT/Vocabulary

: ' Generating vocabulary from train/validation/test dataset
python3 scripts/build_vocabulary.py \
    --GloVe_fpath_in dataset/GloVe/glove.840B.300d.txt \
    --GloVe_fpath_out dataset/GloVe/glove.840B.300d.json \
    --vocabulary_top_k 2000 \
    --dataframe_fpath_list dataset/MSR-VTT/DataFrame/MSR-VTT_train.csv dataset/MSR-VTT/DataFrame/MSR-VTT_val.csv dataset/MSR-VTT/DataFrame/MSR-VTT_test.csv \
    --vocabulary_dpath dataset/MSR-VTT/Vocabulary
'
