python3 scripts/extract_features.py \
    --root_dpath_in MSR-VTT-dataset/frames \
    --fpath_out dataset/MSR-VTT/MSR-VTT_features/RESNET.hdf5 \
    --batch_size 64 \
    --skip_frame 10
