: ' Generating train/test dataset '
python3 scripts/build_dataframe.py \
    --dataset MSR-VTT \
    --data_in_fpath MSR-VTT-dataset/videodatainfo_2017.json \
    --frames_dpath MSR-VTT-dataset/frames \
    --random_seed 42 \
    --corpus_ratio 0.8 0.2 \
    --train_data_out_fpath dataset/MSR-VTT/DataFrame/MSR-VTT_train.csv \
    --test_data_out_fpath dataset/MSR-VTT/DataFrame/MSR-VTT_test.csv \
    --vocabulary_dpath dataset/MSR-VTT/Vocabulary \
    --vocabulary_top_k 2000

: ' Generating train/validation/test dataset
python3 scripts/build_dataframe.py \
    --dataset MSR-VTT \
    --data_in_fpath MSR-VTT-dataset/videodatainfo_2017.json \
    --frames_dpath MSR-VTT-dataset/frames \
    --random_seed 42 \
    --corpus_ratio 0.6 0.2 0.2 \
    --train_data_out_fpath dataset/MSR-VTT/DataFrame/MSR-VTT_train.csv \
    --val_data_out_fpath dataset/MSR-VTT/DataFrame/MSR-VTT_val.csv \
    --test_data_out_fpath dataset/MSR-VTT/DataFrame/MSR-VTT_test.csv \
    --vocabulary_dpath dataset/MSR-VTT/Vocabulary \
    --vocabulary_top_k 2000
'
