import argparse
from ast import literal_eval
import json
import os
import re
from collections import defaultdict, Counter, OrderedDict

import pandas as pd
from tqdm import tqdm


def build_GloVe(args):
    with open(args.GloVe_fpath_in, 'r') as fin:
        GloVe_words = fin.readlines()
    GloVe_words = ( word.split() for word in GloVe_words )

    GloVe = {}
    pbar = tqdm(GloVe_words)
    for word in pbar:
        vocab = "".join(word[:-300])
        pbar.set_description("vocab: {:20s}".format(vocab))
        vector = [ float(e) for e in word[-300:] ]
        GloVe[vocab] = vector
    return GloVe



def load_or_save(fpath, create_func, args):
    if os.path.exists(fpath):
        print("Loading GloVe...")
        with open(fpath, 'r') as fin:
            data = json.load(fin)
    else:
        print("Building GloVe...")
        data = create_func(*args)
        save_as_json(data, fpath)
    return data


def save_as_json(obj, fpath):
    with open(fpath, 'w') as fout:
        json.dump(obj, fout)
    print("Saved {}".format(fpath))


def build_vocabulary(args, GloVe):
    with open("{}/words.json".format(args.vocabulary_dpath), 'r') as fin:
        words = json.load(fin)

    UNK = '<unk>'
    word_matrix = []
    indices = []
    word2idx = {}
    idx2word = {}

    pbar = tqdm(enumerate(words))
    for word_idx, word in pbar:
        pbar.set_description("Processing word \"{}\"...".format(word))
        if word in GloVe:
            word_matrix.append(GloVe[word])
        else:
            print("GloVe doesn't know the word '{}'".format(word))
            word_matrix.append(GloVe[UNK])
        indices.append(word_idx)
        word2idx[word] = word_idx
        idx2word[word_idx] = word

    save_as_json(word_matrix, "{}/word_matrix.json".format(args.vocabulary_dpath))
    save_as_json(word2idx, "{}/word_to_index.json".format(args.vocabulary_dpath))
    save_as_json(idx2word, "{}/index_to_word.json".format(args.vocabulary_dpath))


if __name__ == "__main__":
    a = argparse.ArgumentParser()
    a.add_argument("--GloVe_fpath_in", help="The file path of GloVe input")
    a.add_argument("--GloVe_fpath_out", help="The file path of GloVe output")
    a.add_argument("--vocabulary_top_k", help="The number of extracted vocabulary", type=int)
    a.add_argument("--dataframe_fpath_list", help="The file path of dataframe which contains sentences", nargs='+')
    a.add_argument("--vocabulary_dpath", help="The output directory path")
    args = a.parse_args()

    GloVe = load_or_save(args.GloVe_fpath_out, build_GloVe, [ args ])

    os.makedirs(args.vocabulary_dpath, exist_ok=True)
    build_vocabulary(args, GloVe)

