python3 scripts/extract_features.py \
    --root_dpath_in MSVD-dataset/frames \
    --fpath_out dataset/MSVD/MSVD_features/RESNET.hdf5 \
    --batch_size 64 \
    --skip_frame 10
