from ast import literal_eval
import itertools
import json
import os.path
import random
import sys

import h5py
import numpy as np
import pandas as pd

from videocap.datasets import data_util
from videocap.util import log

__path__ = os.path.abspath(os.path.dirname(__file__))
eos_word = '<EOS>'


def assert_exists(path):
    assert os.path.exists(path), 'Does not exist : {}'.format(path)

# FIXME
MSR_VTT_DATA_DIR = os.path.normpath(os.path.join(__path__, '../../dataset/MSR-VTT'))
assert_exists(MSR_VTT_DATA_DIR)

DATAFRAME_DIR = os.path.join(MSR_VTT_DATA_DIR, 'DataFrame')
assert_exists(DATAFRAME_DIR)

VOCABULARY_DIR = os.path.join(MSR_VTT_DATA_DIR, 'Vocabulary')
assert_exists(VOCABULARY_DIR)

VIDEO_FEATURE_DIR = os.path.join(MSR_VTT_DATA_DIR, 'MSR-VTT_features')
assert_exists(VIDEO_FEATURE_DIR)


class DatasetMSRVTT():
    '''
    Access API for MSR_VTT videos.
    '''

    def __init__(self,
                 dataset_name='train',
                 image_feature_net='resnet',
                 layer='res5c',
                 max_length=80,
                 max_n_videos=None,
                 attr_length=20):
        self.dataset_name = dataset_name
        self.image_feature_net = image_feature_net
        self.layer = layer
        self.max_length = max_length
        self.max_n_videos = max_n_videos
        self.attr_length = attr_length

        self.data_df = self.read_df_from_csvfile()

        if max_n_videos is not None:
            self.data_df = self.data_df[:max_n_videos]
        self.ids = list(self.data_df.index)

        self.feat_h5 = self.read_feat_from_hdf5()

    def __del__(self):
        self.feat_h5.close()

    def __len__(self):
        if self.max_n_videos is not None:
            if self.max_n_videos <= len(self.data_df):
                return self.max_n_videos
        return len(self.data_df)

    def read_feat_from_hdf5(self):
        if self.image_feature_net.lower() == 'resnet':
            if self.layer.lower() == 'res5c':
                feature_file = os.path.join(VIDEO_FEATURE_DIR, self.image_feature_net.upper()+".hdf5")
                assert_exists(feature_file)
            elif self.layer.lower() == 'pool5':
                feature_file = os.path.join(VIDEO_FEATURE_DIR, self.image_feature_net.upper()
                                            + "_" + self.layer.lower() + ".hdf5")
                assert_exists(feature_file)
            elif self.layer.lower() == 'fc100':
                feature_file = os.path.join(VIDEO_FEATURE_DIR, self.image_feature_net.upper()
                                            + "_" + self.layer.lower() + ".hdf5")
                assert_exists(feature_file)
        elif self.image_feature_net.lower() == 'c3d':
            feaure_file = os.path.join(VIDEO_FEATURE_DIR, self.image_feature_net.upper() + ".hdf5")
            assert_exists(feature_file)
        elif self.image_feature_net.lower() == 'google':
            feature_file = os.path.join(VIDEO_FEATURE_DIR, self.image_feature_net.upper() + ".hdf5")
            assert_exists(feature_file)

        log.info("Load %s hdf5 file : %s", self.image_feature_net.upper(), feature_file)

        return h5py.File(feature_file, 'r')

    def read_df_from_csvfile(self):
        train_cap_path = os.path.join(DATAFRAME_DIR, 'MSR-VTT_train.csv')
        test_cap_path = os.path.join(DATAFRAME_DIR, 'MSR-VTT_test.csv')

        train_cap_df = pd.read_csv(train_cap_path, sep='\t')
        test_cap_df = pd.read_csv(test_cap_path, sep='\t')
        self.cap_df = pd.concat([train_cap_df, test_cap_df])
        self.cap_df = self.cap_df.set_index('key')


        if self.dataset_name == 'train':
            data_df = train_cap_df
        elif self.dataset_name == 'test':
            data_df = test_cap_df
        else:
            raise NotImplementedError

        data_df = data_df.set_index('key')
        data_df['row_index'] = range(1, len(data_df)+1)

        return data_df

    @property
    def n_words(self):
        ''' The dictionary size. '''
        if not hasattr(self, 'word2idx'):
            raise Exception('Dictionary not built yet!')
        return len(self.word2idx)

    def __repr__(self):
        if hasattr(self, 'word2idx'):
            return '<DatasetMSRVTT (%s) with %d videos and %d words>' % (
                self.dataset_name, len(self), len(self.word2idx))
        else:
            return '<DatasetMSRVTT (%s) with %d videos -- dictionary not built>' % (
                self.dataset_name, len(self))

    def split_sentence_into_words(self, sentence):
        '''
        Split the given sentence (str) and enumerate the words as strs.
        Each word is normalized, i.e. lower-cased, non-alphabet characters
        like period (.) or comma (,) are stripped.
        When tokenizing, I use ``data_util.clean_str``
        '''
        try:
            words = data_util.clean_str(sentence).split()
        except:
            print(sentence)
            sys.exit()
        words = words + [eos_word]
        for w in words:
            if not w:
                continue
            yield w

    def build_word_vocabulary(self):
        word_matrix_path = os.path.join(VOCABULARY_DIR, 'word_matrix.json')
        assert_exists(word_matrix_path)
        word2idx_path = os.path.join(VOCABULARY_DIR, 'word_to_index.json')
        assert_exists(word2idx_path)
        idx2word_path = os.path.join(VOCABULARY_DIR, 'index_to_word.json')
        assert_exists(idx2word_path)
        index_2000_path = os.path.join(VOCABULARY_DIR, 'index_2000.json')
        assert_exists(index_2000_path)
        word2idx_2000_path = os.path.join(VOCABULARY_DIR, 'word_to_index_2000.json')
        assert_exists(word2idx_2000_path)

        with open(word2idx_2000_path, 'r') as fin:
            self.word2idx_2000 = json.load(fin)
        log.info("Load word2idx_2000 from json file : %s", word2idx_2000_path)

        with open(index_2000_path, 'r') as fin:
            self.index_2000 = json.load(fin)
            self.index_2000 = np.asarray(self.index_2000)
        log.info("Load index_2000 from json file : %s", index_2000_path)

        with open(word_matrix_path, 'r' ) as fin:
            self.word_matrix = json.load(fin)
            self.word_matrix = np.asarray(self.word_matrix)
        log.info("Load word_matrix from json file : %s", word_matrix_path)

        # TODO word2idx file should be dict. now it is list.
        with open(word2idx_path, 'r') as fin:
            self.word2idx = json.load(fin)
        log.info("Load word2idx from json file : %s", word2idx_path)

        with open(idx2word_path, 'r') as fin:
            self.idx2word = json.load(fin)
        log.info("Load idx2word from json file : %s", idx2word_path)

    def share_word_vocabulary_from(self, dataset):
        assert hasattr(dataset, 'idx2word') and hasattr(dataset, 'word2idx'), \
            'The dataset instance should have idx2word and word2idx'
        assert (isinstance(dataset.idx2word, dict) or isinstance(dataset.idx2word, list)) \
                and isinstance(dataset.word2idx, dict), \
            'The dataset instance should have idx2word and word2idx (as dict)'

        if hasattr(self, 'word2idx'):
            log.warn("Overriding %s' word vocabulary from %s ...", self, dataset)

        self.idx2word = dataset.idx2word
        self.word2idx = dataset.word2idx
        self.index_2000 = dataset.index_2000
        self.word2idx_2000 = dataset.word2idx_2000
        if hasattr(dataset, 'word_matrix'):
            self.word_matrix = dataset.word_matrix

    def iter_ids(self, shuffle=False):
        '''
        Iterate data keys. (e.g. vid123..., FIB456..., MC789...,)
        '''
        if shuffle:
            random.shuffle(self.ids)
        for key in self.ids:
            yield key

    def load_video_feature(self, key):
        video_id = self.data_df.loc[key, 'vid_key']
        if self.layer.lower() == 'res5c':
            video_feature = np.array(self.feat_h5[video_id]['res5c'])
        else:
            raise NotImplementedError

        if self.image_feature_net == 'resnet':
            assert self.layer.lower() in ['fc1000', 'pool5', 'res5c']
            if self.layer.lower() == 'res5c':
                video_feature = np.transpose(video_feature, [0, 2, 3, 1])
                assert list(video_feature.shape[1:]) == [7, 7, 2048]
            elif self.layer.lower() == 'pool5':
                video_feature = np.expand_dims(video_feature, axis=1)
                video_feature = np.expand_dims(video_feature, axis=1)
                assert list(video_feature.shape[1:]) == [1, 1, 2048]
            elif self.layer.lower() == 'fc1000':
                video_feature = np.expand_dims(video_feature, axis=1)
                video_feature = np.expand_dims(video_feature, axis=1)
                assert list(video_feature.shape[1:]) == [1, 1, 1000]
        elif self.image_feature_net.lower() == 'c3d':
            assert list(video_feature.shape) == [20, 4096]
            video_feature = np.expand_dims(video_feature, axis=1)
            video_feature = np.expand_dims(video_feature, axis=1)
            assert list(video_feature.shape[1:]) == [1, 1, 4096]
        elif self.image_feature_net.lower() == 'google':
            assert list(video_feature.shape) == [80, 1024]
            video_feature = np.expand_dims(video_feature, axis=1)
            video_feature = np.expand_dims(video_feature, axis=1)
            assert list(video_feature.shape[1:]) == [1, 1, 1024]

        return video_feature

    def get_video_feature_dimension(self):
        if self.image_feature_net == 'resnet':
            assert self.layer.lower() in ['fc1000', 'pool5', 'res5c']
            if self.layer.lower() == 'res5c':
                return (self.max_length, 7, 7, 2048)
            elif self.layer.lower() == 'pool5':
                return (self.max_length, 1, 1, 2048)
            elif self.layer.lower() == 'fc1000':
                return (self.max_length, 1, 1, 1000)
        elif self.image_feature_net.lower() == 'c3d':
            return (self.max_length, 1, 1, 4096)
        elif self.image_feature_net.lower() == 'google':
            return (self.max_length, 1, 1, 1024)
        raise NotImplementedError()

    def get_video_feature(self, key):
        video_feature = self.load_video_feature(key)
        return video_feature

    def convert_sentence_to_matrix(self, sentence):
        '''
        Convert the given sentence into word indices and masks.
        WARNING: Unknown words (not in vocabulary) are revmoed.

        Args:
            sentence: A str for unnormalized sentence, containing T words

        Returns:
            sentence_word_indices : list of (at most) length T,
                each being a word index
        '''
        sent2indices = [self.word2idx[w] for w in
                        self.split_sentence_into_words(sentence)
                        if w in self.word2idx]
        T = len(sent2indices)
        length = min(T, self.max_length)
        return sent2indices[:length]

    def get_video_mask(self, video_feature):
        video_length = video_feature.shape[0]
        return data_util.fill_mask(self.max_length,
                                   video_length,
                                   zero_location='LEFT')

    def get_description(self, key):
        '''
        Return caption string for given key.
        '''
        description = self.data_df.loc[key, 'description']
        return self.convert_sentence_to_matrix(description)


    def get_sentence_mask(self, sentence):
        sent_length = len(sentence)
        return data_util.fill_mask(self.max_length,
                                   sent_length,
                                   zero_location='RIGHT')


    def get_bow(self, key):
        bow_words = self.cap_df.loc[key, 'bow']
        bow_words = literal_eval(bow_words)
        bow_indices = [ self.word2idx_2000[word] for word in bow_words ]
        bow_onehots = np.zeros(len(self.index_2000))
        bow_onehots[bow_indices] = 1.0
        return bow_onehots


    def assemble_into_sentence(self, word_matrix):
        '''
        Convert the word matrix (Batch x MaxLength) into a list of
        human-readable setnences, w.r.t the current directory.
        '''
        B, T = word_matrix.shape
        sentences = [None] * B

        for b in range(B):
            # TODO get eos position
            if 2 in word_matrix[b]:
                eos_position = list(word_matrix[b]).index(2)
            else:
                eos_position = len(word_matrix[b])

            sent = ' '.join(self.idx2word[i] for i in word_matrix[b, :eos_position])
            sentences[b] = sent

        return sentences

    def get_CAP_result(self, chunk):
        batch_size = len(chunk)
        batch_video_feature_convmap = np.zeros([batch_size]
                                               + list(self.get_video_feature_dimension()),
                                               dtype=np.float32)
        batch_caption = np.zeros([batch_size, self.max_length], dtype=np.uint32)
        batch_bow = np.zeros([batch_size, len(self.index_2000)], dtype=np.uint32)

        batch_video_mask = np.zeros([batch_size, self.max_length], dtype=np.uint32)
        batch_caption_mask = np.zeros([batch_size, self.max_length], dtype=np.uint32)

        batch_debug_sent = np.asarray([None] * batch_size)

        for k in range(batch_size):
            key = chunk[k]
            video_feature = self.get_video_feature(key)
            bow = self.get_bow(key)
            video_mask = self.get_video_mask(video_feature)

            batch_video_feature_convmap[k] = data_util.pad_video(video_feature,
                                                                    self.get_video_feature_dimension())
            batch_bow[k] = bow
            batch_video_mask[k] = video_mask

            if self.dataset_name != 'blind':
                try:
                    caption = self.get_description(key)
                    caption_mask = self.get_sentence_mask(caption)
                except:
                    print(key)
                    sys.exit()
                batch_caption[k, :len(caption)] = caption
                batch_caption_mask[k] = caption_mask
                batch_debug_sent[k] = self.data_df.loc[key, 'description']

        ret = {
            'ids': chunk,
            'video_features': batch_video_feature_convmap,
            'caption_words': batch_caption,
            'bow': batch_bow,
            'video_mask': batch_video_mask,
            'caption_mask': batch_caption_mask,
            'debug_sent': batch_debug_sent
        }
        return ret


    def next_batch(self, batch_size=64, include_extra=False, shuffle=True):
        if not hasattr(self, '_batch_it'):
            self._batch_it = itertools.cycle(self.iter_ids(shuffle=shuffle))

        chunk = []
        for k in range(batch_size):
            key = next(self._batch_it)
            chunk.append(key)

        return self.get_CAP_result(chunk)


    def batch_iter(self, num_epochs, batch_size, shuffle=True):
        for epoch in range(num_epochs):
            steps_in_epoch = int(len(self) / batch_size)

            for s in range(steps_in_epoch+1):
                yield self.next_batch(batch_size,
                                      shuffle=shuffle)  # For DEBUG

