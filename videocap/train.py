import time
import os
import sys
sys.path.insert(0, os.path.abspath('.'))
import pprint
import tensorflow as tf
# from videocap.datasets.lsmdc import DatasetLSMDC
from videocap.datasets.msr_vtt import DatasetMSRVTT
from videocap.datasets.msr_vtt import DatasetMSRVTT as DatasetMSVD # TODO: Implement DatasetMSVD
from videocap.datasets import data_util
from videocap.util import log

from videocap.models.caption_model import CAPGenerator, CAPTrainer
from videocap.models.fib_model import FIBGenerator, FIBTrainer
from videocap.datasets.batch_queue import BatchQueue
from videocap.configuration import ModelConfig, TrainConfig
from videocap.slack import Slack
import json

# For debug purporses
import hickle as hkl
import numpy as np
pp = pprint.PrettyPrinter(indent=2)


def main(argv):
    slack_token = os.environ['SLACK_TOKEN']
    slack = Slack(slack_token)

    model_config = ModelConfig()
    train_config = TrainConfig()

    Dataset = DatasetMSRVTT if train_config.dataset == 'MSR-VTT' else DatasetMSVD
    train_dataset = Dataset(dataset_name='train',
                            image_feature_net=model_config.image_feature_net,
                            layer=model_config.layer,
                            max_length=model_config.video_steps,
                            max_n_videos=None,
                            attr_length=model_config.attr_length)
    test_dataset = Dataset(dataset_name='test',
                                 image_feature_net=model_config.image_feature_net,
                                 layer=model_config.layer,
                                 max_length=model_config.video_steps,
                                 max_n_videos=None,
                                 attr_length=model_config.attr_length)
    train_dataset.build_word_vocabulary()
    test_dataset.share_word_vocabulary_from(train_dataset)

    train_iter = train_dataset.batch_iter(train_config.num_epochs, model_config.batch_size)
    train_queue = BatchQueue(train_iter, name='train')
    val_iter = test_dataset.batch_iter(20*train_config.num_epochs, model_config.batch_size, shuffle=False)
    val_queue = BatchQueue(val_iter, name='test')
    train_queue.start_threads()
    val_queue.start_threads()

    g = tf.Graph()
    with g.as_default():
        global session, model, trainer
        tf_config = tf.ConfigProto()
        tf_config.gpu_options.allow_growth = True
        session = tf.Session(graph=g, config=tf_config)

        model = CAPGenerator(model_config, train_dataset.word_matrix,
                                               train_dataset.index_2000)

        log.info("Build the model...")
        model.build_model(**model.get_placeholder())
        trainer = CAPTrainer(train_config, model, session)

        session.run(tf.global_variables_initializer())
        steps_in_epoch = int(np.ceil(len(train_dataset) / model.batch_size))

        for step in range(1, train_config.max_steps+1):
            step_result = trainer.run_single_step(
                queue=train_queue, is_train=True)

            if step_result['current_step'] % train_config.steps_per_logging == 0:
                step_result['steps_in_epoch'] = steps_in_epoch
                trainer.log_step_message(**step_result)

            if step_result['current_step'] % train_config.steps_per_evaluate_single == 0:
                loss, concept_loss, output_concepts, target_concepts, output_captions, target_captions = trainer.eval_single_step(
                        queue=val_queue, dataset=test_dataset, n_logs=1)
                slack_message = "```"
                slack_message += "[Iteration {}/{}]\n".format(step, train_config.max_steps)
                slack_message += "loss: {}\tconcept loss: {}\n\n".format(loss, concept_loss)
                slack_message += "[output concepts]\n\t{}\n".format(output_concepts)
                slack_message += "[target concepts]\n\t{}\n\n".format(target_concepts)
                slack_message += "[output captions]\n\t{}\n".format(output_captions)
                slack_message += "[target captions]\n\t{}".format(target_captions)
                slack_message += "```"
                slack.send_message(slack_message)

            if step_result['current_step'] % train_config.steps_per_evaluate == 0:
                trainer.evaluate(queue=val_queue, dataset=test_dataset)

        train_queue.thread_close()
        val_queue.thread_close()

if __name__ == '__main__':
    tf.app.run(main=main)
